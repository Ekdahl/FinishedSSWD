﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemLibDTO;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel.DataAnnotations;

namespace SystemLibDAL
{
    public class dataAccess
    {
        public static List<UserDTO> getAllBorrowers()
        {
            List<UserDTO> borrowerList = new List<UserDTO>();
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            string SQL = "SELECT * FROM Borrower";
            SqlCommand cmd = new SqlCommand(SQL, con);
            try
            {
                con.Open();
                SqlDataReader dar = cmd.ExecuteReader();
                while (dar.Read())
                {
                    UserDTO uDTO = new UserDTO();
                    uDTO._Address = (dar["Address"] == DBNull.Value) ? "[N/A]" :dar["Address"] as string;
                    uDTO._Authorization = (dar["Auth"] == DBNull.Value) ? -1: Convert.ToInt32(dar["Auth"]);
                    uDTO._CategoryID = (dar["CategoryId"] == DBNull.Value) ? 999 : Convert.ToInt32(dar["CategoryId"]);
                    uDTO._LastName = (dar["LastName"] == DBNull.Value) ? "[N/A]" : dar["LastName"] as string;
                    uDTO._FirstName = (dar["FirstName"] == DBNull.Value) ? "[N/A]" : dar["FirstName"] as string;
                    uDTO._PersonId = (dar["PersonId"] == DBNull.Value) ? "[N/A]" : dar["PersonId"] as string;
                    uDTO._Telno = (dar["TelNo"] == DBNull.Value) ? "[N/A]" : dar["TelNo"] as string;
                    borrowerList.Add(uDTO);
                }
            }
            catch (Exception er)
            {
                throw er;
            }
            finally
            {
                con.Close();
            }
            return borrowerList;
        }

        public static void delete(int id)
        {
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);

            SqlCommand[] commands = new SqlCommand[4];
            commands[0] = new SqlCommand("SELECT * FROM BOOK_AUTHOR WHERE Aid=" + id, con);
            commands[1] = new SqlCommand("DELETE FROM BOOK_AUTHOR WHERE Aid=" + id, con);
            commands[2] = new SqlCommand("DELETE FROM AUTHOR WHERE Aid=" + id, con);

            List<string> ISBNList = new List<string>();
            List<BookDTO> bookList = new List<BookDTO>();

            for (int i = 0; i < 4; i++)
            {
                try
                {
                    con.Open();
                    if (i == 0)
                    {
                        SqlDataReader dar = commands[i].ExecuteReader();

                        while (dar.Read())
                        {
                            ISBNList.Add((dar["ISBN"] == DBNull.Value) ? "" : dar["ISBN"] as string);
                        }
                        foreach (var isbn in ISBNList)
                        {
                            bookList.Add(getBook(isbn));
                        }
                    }
                    else if (i == 3)
                    {
                        foreach (var book in bookList)
                        {
                            deleteBook(book.ISBN);
                        }
                    }
                    else
                    {
                        commands[i].ExecuteScalar();
                    }
                   
                }
                catch (Exception eObj)
                {
                    throw eObj;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public static void deleteBook(string ISBN)
        {
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            CopyDTO copy = new CopyDTO();
            SqlCommand cmd = new SqlCommand("SELECT * FROM COPY WHERE ISBN='" + ISBN + "'", con);
            SqlCommand cmd2 = new SqlCommand();
            SqlCommand cmd3 = new SqlCommand("DELETE FROM BOOK_AUTHOR WHERE ISBN='" + ISBN + "'", con);
            SqlCommand cmd4 = new SqlCommand("DELETE FROM Book WHERE ISBN='" + ISBN + "'", con);

            try
            {
                con.Open();
                SqlDataReader dar = cmd.ExecuteReader();
                while (dar.Read())
                {
                    copy.ISBN = (dar["ISBN"] == DBNull.Value) ? "" : dar["ISBN"] as string;
                    copy.Barcode = (dar["Barcode"] == DBNull.Value) ? "" : dar["Barcode"] as string;
                    cmd2 = new SqlCommand("DELETE FROM COPY WHERE Barcode='" + copy.Barcode + "'", con);
                }

            }
            catch (Exception eObj)
            {
                
                throw eObj;
            }
            finally
            {
                con.Close();
            }

            try
            {
                con.Open();
                if (!copy.Barcode.Equals("") && !copy.Barcode.Equals(null) && !copy.ISBN.Equals("") && !copy.ISBN.Equals(null))
                {
                    cmd2.ExecuteScalar();
                }

            }
            catch (Exception eObj)
            {
                throw eObj;
            }
            finally
            {
                con.Close();
            }

            try
            {
                con.Open();
                cmd3.ExecuteScalar();
            }
            catch (Exception eObj)
            {
                throw eObj;
            }
            finally
            {
                con.Close();
            }

            try
            {
                con.Open();
                cmd4.ExecuteScalar();
            }
            catch (Exception eObj)
            {
                throw eObj;
            }
            finally
            {
                con.Close();
            }

        }

        public static void deleteUser(string personId)
        {
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("DELETE FROM BORROWER WHERE PersonId='" + personId + "'", con);
            SqlCommand cmd2 = new SqlCommand("DELETE FROM BORROW WHERE PersonId='" + personId + "'", con);

            try
            {
                con.Open();
                cmd2.ExecuteScalar();
                cmd.ExecuteScalar();
            }
            catch (Exception eObj)
            {
                throw eObj;
            }
            finally
            {
                con.Close();
            }
        }

        public static void updateBorrower(UserDTO borrowerToUpdate)
        {
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("UPDATE Borrower set FirstName='" + borrowerToUpdate._FirstName + "', LastName='" + borrowerToUpdate._LastName + "', Address='" + borrowerToUpdate._Address +"', Auth=" + borrowerToUpdate._Authorization+ ", CategoryId = "+ borrowerToUpdate._CategoryID + ", TelNo = '"+ borrowerToUpdate._Telno + "'  WHERE PersonId='" + borrowerToUpdate._PersonId + "'", con);
           

            try
            {
                con.Open();
                cmd.ExecuteScalar();  
            }
            catch (Exception er)
            {
                throw er;
            }
            finally
            {
                con.Close();
            }
        }

        public static int saveAuthor(AuthorDTO ad)
        {
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd;

            int ret = -1;
            bool existing;


            if (ad._aId == 0)
            {
                existing = false;
                cmd = new SqlCommand("INSERT INTO AUTHOR (FirstName, LastName, BirthYear) VALUES ( '" + ad._firstName + "','" + ad._lastName + "','" + Convert.ToString(ad._birthYear) + "'); SELECT SCOPE_IDENTITY()", con);
            }
            else
            {
                existing = true;
                cmd = new SqlCommand("UPDATE AUTHOR set FirstName='" + ad._firstName + "', LastName='" + ad._lastName + "', BirthYear=" + ad._birthYear + " WHERE Aid=" + ad._aId, con);
            }
            try
            {
                con.Open();
                if (!existing)
                {

                    ret = cmd.ExecuteNonQuery();
                }
                else
                {
                    ret = -1;
                    cmd.ExecuteScalar();
                }

            }
            catch (Exception er)
            {
                throw er;
            }
            finally
            {
                con.Close();
            }
            return ret;
        }
        public static int saveBook(BookDTO bd)
        {
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand[] commands = new SqlCommand[3];
            int AuthorId = 0;
            commands[0] = new SqlCommand("SELECT * FROM AUTHOR WHERE FirstName='" + bd.AutorFirstName + "' AND LastName='" + bd.AuthorLastName + "'", con);
           
            //SqlCommand cmd2;
            

            int ret = -1;
            bool existing;
            BookDTO ifBookExist = getBook(bd.ISBN);
          
                if (ifBookExist.ISBN != bd.ISBN)
                {
                    existing = false;
                    commands[1] = new SqlCommand("INSERT INTO BOOK (ISBN, pages, Title, publicationinfo, PublicationYear, SignId) VALUES ( '" + bd.ISBN + "'," + Convert.ToInt16(bd.Pages) + ", '" + bd.Title + "','" + bd.PublicationInfo + "','" + bd.PublicationYear + "'," + bd.SignID + "); SELECT SCOPE_IDENTITY()", con);

                }
                else
                {
                    existing = true;
                    commands[1] = new SqlCommand("UPDATE BOOK set ISBN='" + bd.ISBN + "', Title='" + bd.Title + "', publicationinfo='" + bd.PublicationInfo + "', PublicationYear='" + bd.PublicationYear + "', SignId='" + bd.SignID + "' WHERE ISBN='" + bd.ISBN + "'", con);
                }
            
           
           
           
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    con.Open();
                    if (i == 0)
                    {
                        SqlDataReader dar = commands[i].ExecuteReader();
                        if(dar.Read())
                        {
                            AuthorId = (dar["Aid"] == DBNull.Value) ? 000 : Convert.ToInt32(dar["Aid"]);
                            if (AuthorId == 000)
                            {
                                con.Close();
                                return -4;
                            }
                            commands[2] = new SqlCommand("INSERT INTO BOOK_AUTHOR (ISBN, Aid) VALUES ('" + bd.ISBN + "'," + AuthorId + ")", con);
                        }
                        else
                        {
                            con.Close();
                            //No such Author exists
                            return -4;
                        }
                    }
                    else if (i == 1)
                    {
                        
                        if (!existing)
                        {
                            ret = commands[i].ExecuteNonQuery();
                        }
                        else
                        {
                            ret = -1;
                            commands[i].ExecuteScalar();
                        }
                    }
                    else
                    {
                        commands[i].ExecuteNonQuery();
                    }

                }
                catch (Exception er)
                {
                    throw er;
                }
                finally
                {
                    con.Close();
                }
            }
            return ret;
        }

        public static int saveBorrower(UserDTO ud)
        {

            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd;

            int ret = -1;
            bool existing;

            UserDTO ifuserExist = getUser(ud._PersonId);
            if (ifuserExist._PersonId != ud._PersonId)
            {
                existing = false;
                cmd = new SqlCommand("INSERT INTO BORROWER (PersonId ,FirstName, LastName, Address, Telno, CategoryId, Password, Salt, Auth) VALUES ( '" + ud._PersonId + "','" + ud._FirstName + "','" + ud._LastName + "','" + ud._Address + "','" + ud._Telno + "'," + ud._CategoryID + ",'" + ud._Password + "','" + ud._Salt + "'," + ud._Authorization + "); SELECT SCOPE_IDENTITY()", con);
            }
            else
            {

                existing = true;
                cmd = new SqlCommand("UPDATE BORROWER set FirstName='" + ud._FirstName + "', LastName='" + ud._LastName + "', Address='" + ud._Address + "', Telno='" + ud._Telno + "', CategoryId=" + ud._CategoryID + ", Auth=" + ud._Authorization + " WHERE PersonId='" + ud._PersonId + "'", con);
            }
            try
            {
                con.Open();
                if (!existing)
                {

                    ret = cmd.ExecuteNonQuery();
                }
                else
                {
                    ret = -1;
                    cmd.ExecuteScalar();
                }

            }
            catch (Exception er)
            {
                throw er;
            }
            finally
            {
                con.Close();
            }
            return ret;
        }


        public static int borrowBook(string ISBN, string personId, DateTime borrowDate, DateTime toBeReturned)
        {
            string barcode = RandomizeBarcode();
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd;
            SqlCommand cmd2;

            int ret = -1;
            int ret2 = -1;
            bool existing;

            // BookDTO ifBookAlreadyBorrowed = getUser(ud._PersonId);
            if (barcode != "")
            {
                existing = false;
                cmd = new SqlCommand("INSERT INTO COPY (Barcode, ISBN) VALUES ('" + barcode + "','" + ISBN + "'" + "); SELECT SCOPE_IDENTITY()", con);
                cmd2 = new SqlCommand("INSERT INTO BORROW (Barcode , PersonId, BorrowDate, ToBeReturnedDate) VALUES ( '" + barcode + "','" + personId + "','" + borrowDate + "','" + toBeReturned + "'" + "); SELECT SCOPE_IDENTITY()", con);
            }
            else
            {

                existing = true;
                cmd = new SqlCommand("UPDATE COPY set Barcode='" + barcode + "', ISBN='" + ISBN + "'" + " WHERE Barcode='" + barcode + "'", con);
                cmd2 = new SqlCommand("UPDATE BORROW set Barcode='" + barcode + "', PersonId='" + personId + "', BorrowDate=" + borrowDate + ", ToBeReturnedDate=" + toBeReturned + " WHERE Barcode='" + barcode + "'", con);
            }
            try
            {
                con.Open();
                if (!existing)
                {

                    ret = cmd.ExecuteNonQuery();
                    ret2 = cmd2.ExecuteNonQuery();
                }
                else
                {
                    ret = -1;
                    cmd.ExecuteScalar();

                }

            }
            catch (Exception er)
            {
                throw er;
            }
            finally
            {
                con.Close();
            }
            return ret;
        }


        public static int renewLoan(string ISBN)
        {
            string barcode = "";
            DateTime borrowDate = new DateTime();
            string oldToBeReturnedDateStr;
            DateTime oldToBeReturnedDate = new DateTime();
            CopyDTO copy = new CopyDTO();
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);

            SqlCommand cmd2 = new SqlCommand();
            SqlCommand cmd3 = new SqlCommand() ;
                

            int ret = -1;
            int ret2 = -1;
            int ret3 = -1;
          
            try
            {
                con.Open();
                if (ISBN != "")
                {
                    copy = getCopy(ISBN);
                    cmd2 = new SqlCommand("SELECT * FROM BORROW WHERE Barcode='" + copy.Barcode + "'", con);

                    SqlDataReader dar2 = cmd2.ExecuteReader();
                    if (dar2.Read())
                    {
                        oldToBeReturnedDate = Convert.ToDateTime(dar2["ToBeReturnedDate"]);
                        barcode = dar2["Barcode"] as string;
                        borrowDate = Convert.ToDateTime(dar2["BorrowDate"]);
                        cmd3 = new SqlCommand("UPDATE BORROW set ToBeReturnedDate='" + oldToBeReturnedDate.AddDays(7) + "' WHERE Barcode='" + copy.Barcode + "'", con);
                    }
                   
                }
                else
                {
                    ret = -1;
                }

            }
            catch (Exception er)
            {
                throw er;
            }
            finally
            {
                con.Close();
            }

            try
            {
                con.Open();
                ret3 = cmd3.ExecuteNonQuery(); 
            }
            catch(Exception er)
            {
                throw er;
            }
            finally
            {
                con.Close();
            }

            return ret3;
        }



        public static List<BookDTO> fetchCurrentlyBorrowedBooks(string personId)
        {
             
            List<string> barcodeList = new List<string>();
            List<BookDTO> bookList = new List<BookDTO>();
            List<CopyDTO> copyList = new List<CopyDTO>();
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd;
            try
            {
                con.Open();
                cmd = new SqlCommand("SELECT * FROM BORROW WHERE PersonId='" + personId + "' AND ToBeReturnedDate >='" + DateTime.Now + "'", con);
                //AND ToBeReturnedDate <'" + DateTime.Now + "'
                SqlDataReader dar = cmd.ExecuteReader();
                while (dar.Read())
                {
                    BookDTO bDTO = new BookDTO();
                    string barcode = dar["Barcode"] as string;
                    barcodeList.Add(barcode);
                }
            }
            catch (Exception er)
            {
                throw er;
            }
            finally
            {
                con.Close();
            }

            foreach(var barcode in barcodeList)
            {
                CopyDTO copy = new CopyDTO();
                copyList.Add(copy = getCopy(barcode));
            }
            foreach(var copy in copyList)
            {
                BookDTO currentlyborrowedBook = new BookDTO();
                bookList.Add(currentlyborrowedBook = getBook(copy.ISBN));
            }

            return bookList;
        }

        public static CopyDTO getCopy(string barcode)
        {

            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd;
            CopyDTO copy = new CopyDTO();
            try
            {
                con.Open();
                cmd = new SqlCommand("SELECT * FROM COPY WHERE Barcode='" + barcode + "'", con);
                SqlDataReader dar = cmd.ExecuteReader();

                if (dar.Read())
                {
                    copy.ISBN = (dar["ISBN"] == DBNull.Value) ? "No ISBN" : dar["ISBN"] as string;
                    copy.Barcode = (dar["Barcode"] == DBNull.Value) ? "No barcode" : dar["Barcode"] as string;
                    copy.Library = (dar["Library"] == DBNull.Value) ? "No Lib" : dar["Library"] as string;
                    copy.Location = (dar["Location"] == DBNull.Value) ? "No Location" : dar["Location"] as string;
                    copy.StatusId = (dar["StatusId"] == DBNull.Value) ? 999 : (int)dar["StatusId"];
                }

            
            }
            catch (Exception er)
            {
                throw er;
            }
            finally
            {
                con.Close();
            }

            return copy;

        }


        public static AuthorDTO getAuthor(int aId)
        {
            AuthorDTO dto = null;
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("SELECT * FROM author WHERE aid =@Aid", con);
            SqlParameter param = new SqlParameter("Aid", SqlDbType.Int);
            param.Value = aId;
            cmd.Parameters.Add(param);
            try
            {
                con.Open();
                SqlDataReader dar = cmd.ExecuteReader();
                if (dar.Read())
                {
                    dto = new AuthorDTO();
                    dto._aId = (int)dar["Aid"];
                    dto._lastName = dar["LastName"] as string;
                    dto._firstName = dar["FirstName"] as string;
                    dto._birthYear = (dar["BirthYear"] == DBNull.Value) ? 0 : Convert.ToInt32(dar["BirthYear"].ToString());
                }
            }
            catch (Exception eObj)
            {
                throw eObj;
            }
            finally
            {
                con.Close();
            }
            return dto;
        }

        public static List<AuthorDTO> getAuthors(string SQL)
        {
            List<AuthorDTO> authorList = new List<AuthorDTO>();
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand(SQL, con);
            try
            {
                con.Open();
                SqlDataReader dar = cmd.ExecuteReader();
                while (dar.Read())
                {
                    AuthorDTO aDTO = new AuthorDTO();
                    aDTO._aId = (int)dar["Aid"];
                    aDTO._birthYear = (dar["BirthYear"] == DBNull.Value) ? 0 : Convert.ToInt32(dar["BirthYear"] as string);
                    aDTO._firstName = dar["FirstName"] as string;
                    aDTO._lastName = dar["LastName"] as string;
                    authorList.Add(aDTO);
                }
            }
            catch (Exception er)
            {
                throw er;
            }
            finally
            {
                con.Close();
            }
            return authorList;
        }

        public static List<BookDTO> getBooks(string SQL)
        {
        
            List<BookDTO> bookList = new List<BookDTO>();
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand(SQL, con);
            try
            {
                con.Open();
                SqlDataReader dar = cmd.ExecuteReader();
                while (dar.Read())
                {
                    BookDTO bDTO = new BookDTO();
                    bDTO.Title = dar["Title"] as string;
                    bDTO.SignID = (int)dar["SignId"];
                    bDTO.Pages = Convert.ToInt16(dar["pages"]);
                    bDTO.ISBN = dar["ISBN"] as string;
                    bDTO.PublicationInfo = dar["publicationinfo"] as string;
                    bDTO.PublicationYear = (dar["PublicationYear"] == DBNull.Value) ? "0" : dar["PublicationYear"] as string;
                    bDTO.AuthorLastName = dar["LastName"] as string;
                    bDTO.AutorFirstName = dar["FirstName"] as string;
                    bookList.Add(bDTO);
                }
            }
            catch (Exception er)
            {
                throw er;
            }
            finally
            {
                con.Close();
            }
            return bookList;
        }

        public static int createUser(UserDTO uDTO)
        {
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("INSERT INTO BORROWER (PersonId ,FirstName, LastName, Address, Telno, CategoryId, Password, Salt, Auth) VALUES ( '" + uDTO._PersonId + "','" + uDTO._FirstName + "','" + uDTO._LastName + "','" + uDTO._Address + "','" + uDTO._Telno + "'," + uDTO._CategoryID + ",'" + uDTO._Password + "','" + uDTO._Salt + "'," + uDTO._Authorization + "); SELECT SCOPE_IDENTITY()", con);
            bool ok = false;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;

            }
            catch (Exception er)
            {
                throw er;
            }
            finally
            {
                con.Close();
            }
            return 1;
        }
        public static UserDTO getUser(string PersonId)
        {
            string SQL = "SELECT * FROM BORROWER WHERE PersonId='" + PersonId + "'";
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand(SQL, con);
            UserDTO uDTO = new UserDTO();
            try
            {
                con.Open();
                SqlDataReader dar = cmd.ExecuteReader();
                if (dar.Read())
                {
                    
                    uDTO._PersonId = dar["PersonId"] as string;  //Null not allowed in db
                    uDTO._FirstName = (dar["FirstName"] == DBNull.Value) ? "No name" : dar["FirstName"] as string;
                    uDTO._LastName = (dar["LastName"] == DBNull.Value) ? "No name" : dar["LastName"] as string;
                    uDTO._Telno = (dar["Telno"] == DBNull.Value) ? "No tel number" : dar["Telno"] as string;
                    uDTO._Authorization = (dar["Auth"] == DBNull.Value) ? 999 : (int)dar["Auth"];
                    uDTO._Address = (dar["Address"] == DBNull.Value) ? "No address" : dar["Address"] as string;
                    uDTO._CategoryID = (dar["CategoryId"] == DBNull.Value) ? 999 : (int)dar["CategoryId"];
                    uDTO._Password = (dar["Password"] == DBNull.Value) ? "null" : dar["Password"] as string;
                    uDTO._Salt = (dar["Salt"] == DBNull.Value) ? "null" : dar["Salt"] as string;

                }
            }
            catch (Exception er)
            {
                throw er;
            }
            finally
            {
                con.Close();
            }
            return uDTO;
        }
        public static BookDTO getBook(string ISBN)
        {
            string SQL = "SELECT * FROM BOOK WHERE ISBN ='" + ISBN + "'";
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand(SQL, con);
            BookDTO bDTO = new BookDTO();
            try
            {
                con.Open();
                SqlDataReader dar = cmd.ExecuteReader();
                if (dar.Read())
                {

                    bDTO.Title= dar["Title"] as string; 
                    bDTO.Pages =  (short)dar["Pages"];
                    bDTO.ISBN = (dar["ISBN"] == DBNull.Value) ? "" : dar["ISBN"] as string;
                    bDTO.PublicationInfo = (dar["publicationinfo"] == DBNull.Value) ? "No tel number" : dar["publicationinfo"] as string;
                    bDTO.PublicationYear = (dar["PublicationYear"] == DBNull.Value) ? "no pub year" : dar["PublicationYear"] as string;
                    bDTO.SignID = (dar["SignId"] == DBNull.Value) ? 000 : (int)dar["SignId"];
                    

                }
            }
            catch (Exception er)
            {
                throw er;
            }
            finally
            {
                con.Close();
            }
            return bDTO;
        }

        public static List<UserDTO> getUsers(string SQL)
        {

            List<UserDTO> userList = new List<UserDTO>();
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand(SQL, con);
            try
            {
                con.Open();
                SqlDataReader dar = cmd.ExecuteReader();
                while (dar.Read())
                {

                    UserDTO uDTO = new UserDTO();
                    uDTO._PersonId = dar["PersonId"] as string;  //Null not allowed in db
                    uDTO._FirstName = (dar["FirstName"] == DBNull.Value) ? "No name" : dar["FirstName"] as string;
                    uDTO._LastName = (dar["LastName"] == DBNull.Value) ? "No name" : dar["LastName"] as string;
                    uDTO._Telno = (dar["Telno"] == DBNull.Value) ? "No tel number" : dar["Telno"] as string;
                    uDTO._Authorization = (dar["Auth"] == DBNull.Value) ? 999 : (int)dar["Auth"];
                    uDTO._Address = (dar["Address"] == DBNull.Value) ? "No address" : dar["Address"] as string;
                    uDTO._CategoryID = (dar["CategoryId"] == DBNull.Value) ? 999 : (int)dar["CategoryId"];
                    userList.Add(uDTO);
                }
            }
            catch (Exception er)
            {
                throw er;
            }
            finally
            {
                con.Close();
            }
            return userList;
        }

        public static bool ValidateUser(string PersonId, string hashedPassword)
        {
            string SQL = "SELECT * FROM BORROWER WHERE PersonId='" + PersonId + "'";
            string _connectionString = DataSource.GetConnectionString("AdminConnection");
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand(SQL, con);
            string dbHashPwd = "";

            try
            {
                con.Open();
                SqlDataReader dar = cmd.ExecuteReader();
                if (dar.Read())
                {
                    dbHashPwd = dar["Password"] as string;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
            }
            if (hashedPassword == dbHashPwd)
            {
                return true;
            }
            else
                return false;
        }

        private static string RandomizeBarcode()
        {
            string numbers = "0123456789";

            Random random = new Random();
            return new string(Enumerable.Repeat(numbers, 20).Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
