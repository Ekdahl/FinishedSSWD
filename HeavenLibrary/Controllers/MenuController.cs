﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SystemLibBL;
using SystemLibDTO;

namespace HeavenLibrary.Controllers
{
    public class MenuController : Controller
    {
        //
        // GET: /Menu/

        public void LogIn(string username, string password)
        {

            string SQL = "SELECT * FROM Users WHERE UserName='"+username+"'";
            string g = "";
        }
        public ActionResult LogOut()
        {
            Session.Abandon();
            ViewBag.showLoginPanel = true;
            return RedirectToAction("Index");
        }
        public ActionResult Widget()
        {
            return View();
        }

        public ActionResult Index()
        {
            if (Session["User"] != null)
            {
                SystemLibBL.User user = (SystemLibBL.User)Session["User"];
                ViewBag.userName = user.PersonId;
                ViewBag.showLoginPanel = false;

            }
            else
            {
                ViewBag.showLoginPanel = true;
            }

            
            return View(SystemLibBL.Author.getAllAuthors());
        }
        public ActionResult PublicAuthorDetails(int id)
        {
            return View(SystemLibBL.Author.findAuthor(id));
        }

        public ActionResult ManageBorrowers()
        {
            return View(SystemLibBL.User.getAllBorrowers());
        }

        public ActionResult PublicBooks()
        {
            if (Session["User"] != null)
            {
                  SystemLibBL.User user = (SystemLibBL.User)Session["User"];
                ViewBag.userName = user.PersonId;
                ViewBag.showLoginPanel = false;

            }
            else
            {
                ViewBag.showLoginPanel = true;
            }

            return View(SystemLibBL.Book.getBooks());
        }
        public ActionResult AdminLogin()
        {
            if (Session["User"] != null)
            {
                SystemLibBL.User user = (SystemLibBL.User)Session["User"];
                ViewBag.userName = user.PersonId;
                ViewBag.showLoginPanel = false;
                return View(SystemLibBL.Book.getBooks());
            }

            return RedirectToAction("Index", "Menu");
            
        }

        public ActionResult BorrowerLogin()
        {
            if (Session["User"] != null)
            {
                SystemLibBL.User user = (SystemLibBL.User)Session["User"];
                ViewBag.userName = user.PersonId;
                ViewBag.showLoginPanel = false;

                return View(SystemLibBL.Book.getBooks());

            }
           
            return RedirectToAction("Index", "Menu");
        }

        
        public ActionResult StartPage()
        {
            if (Session["User"] != null)
            {
                SystemLibBL.User user = (SystemLibBL.User)Session["User"];
                ViewBag.userName = user.PersonId;
                ViewBag.showLoginPanel = false;
            }
            else
            {
                ViewBag.showLoginPanel = true;
            }
            return View();
        }

        public ActionResult Search()
        {
            //SystemLibDTO.UserDTO uDTO = new SystemLibDTO.UserDTO();
            //uDTO._PersonId = "19931004-6975";
            //uDTO._FirstName = "Axel";
            //uDTO._LastName = "Ekdahl";
            //uDTO._Address = "Dunkehallavägen 78";
            //uDTO._Telno = "0709566660";
            //uDTO._Authorization = 2;
            //uDTO._Password = "password";
            //SystemLibBL.User.createUser(uDTO);

            string searchPhrase = Request.QueryString.Get("SearchPhrase");
            string dropValue = Request.QueryString.Get("dropList");
            //Searched after book title
            if (dropValue == "Boo")
            {
                if (Session["User"] != null)
                {
                    SystemLibBL.User user = (SystemLibBL.User)Session["User"];
                    ViewBag.userName = user.PersonId;
                    ViewBag.showLoginPanel = false;
                }
                else
                {
                    ViewBag.showLoginPanel = true;
                }
                return View(SystemLibBL.Book.searchBook("SELECT BOOK.*, AUTHOR.FirstName, AUTHOR.LastName FROM BOOK JOIN BOOK_AUTHOR ON BOOK.ISBN = BOOK_AUTHOR.ISBN JOIN AUTHOR ON BOOK_AUTHOR.Aid = AUTHOR.Aid WHERE Title LIKE '" + searchPhrase + "%' OR Title LIKE '%" + searchPhrase + "%'"));
            }

            //Searched after author name
            else
            {
                if (Session["User"] != null)
                {
                    SystemLibBL.User user = (SystemLibBL.User)Session["User"];
                    ViewBag.userName = user.PersonId;
                    ViewBag.showLoginPanel = false;
                }
                else
                {
                    ViewBag.showLoginPanel = true;
                }
                return View(SystemLibBL.Book.searchBook("SELECT BOOK.*, AUTHOR.FirstName, AUTHOR.LastName FROM BOOK JOIN BOOK_AUTHOR ON BOOK.ISBN = BOOK_AUTHOR.ISBN JOIN AUTHOR ON BOOK_AUTHOR.Aid = AUTHOR.Aid WHERE AUTHOR.FirstName LIKE '" + searchPhrase + "%' OR AUTHOR.LastName LIKE '" + searchPhrase + "%' OR AUTHOR.FirstName LIKE '%" + searchPhrase + "%' OR AUTHOR.LastName LIKE '%" + searchPhrase + "%'"));
            }
        }
        public ActionResult AdminSearch()
        {
           

            string searchPhrase = Request.QueryString.Get("SearchPhrase");
            string dropValue = Request.QueryString.Get("dropList");

            if (dropValue == "Boo")
            {
                if (Session["User"] != null)
                {
                    SystemLibBL.User user = (SystemLibBL.User)Session["User"];
                    ViewBag.userName = user.PersonId;
                    ViewBag.showLoginPanel = false;
                }
                else
                {
                    ViewBag.showLoginPanel = true;
                }
                return View(SystemLibBL.Book.searchBook("SELECT BOOK.*, AUTHOR.FirstName, AUTHOR.LastName FROM BOOK JOIN BOOK_AUTHOR ON BOOK.ISBN = BOOK_AUTHOR.ISBN JOIN AUTHOR ON BOOK_AUTHOR.Aid = AUTHOR.Aid WHERE Title LIKE '" + searchPhrase + "%' OR Title LIKE '%" + searchPhrase + "%'"));
            }
            else if (dropValue == "Borrower")
            {
                if (Session["User"] != null)
                {
                    SystemLibBL.User user = (SystemLibBL.User)Session["User"];
                    ViewBag.userName = user.PersonId;
                    ViewBag.showLoginPanel = false;
                }
                else
                {
                    ViewBag.showLoginPanel = true;
                }

                return View("AdminSearchBorrower", SystemLibBL.User.searchUser("SELECT * FROM BORROWER "));
                //"AdminSearchBorrower", SystemLibBL.User.searchUser("SELECT * FROM BORROWER ")

            }
            else
            {
                if (Session["User"] != null)
                {
                    SystemLibBL.User user = (SystemLibBL.User)Session["User"];
                    ViewBag.userName = user.PersonId;
                    
                    ViewBag.showLoginPanel = false;
                }
                else
                {
                    ViewBag.showLoginPanel = true;
                }
                return View("AdminSearchAuthor",SystemLibBL.Author.searchAuthor("SELECT * FROM AUTHOR WHERE FirstName LIKE '" + searchPhrase + "%' OR LastName LIKE '" + searchPhrase + "%' OR FirstName LIKE '%" + searchPhrase + "%' OR LastName LIKE '%" + searchPhrase + "%'"));
            }
        }
    
        //
        // GET: /Menu/Details/5

        public ActionResult Details(int id)
        {
            return View(SystemLibBL.Author.findAuthor(id));
        }
        public ActionResult PublicDetailsBook(string ISBN)
        {
            return View(SystemLibBL.Book.findBook(ISBN));
        }

        public ActionResult DetailsBook(string ISBN)
        {
            return View(SystemLibBL.Book.findBook(ISBN));
        }

        public ActionResult DetailsBorrower(string personId)
        {
            return View(SystemLibBL.User.findUser(personId));
        }
        //
        // GET: /Menu/Create

        public ActionResult Create()
        {
            return View();

        }

        //
        // POST: /Menu/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                SystemLibBL.Author author = new SystemLibBL.Author();
                author.BirthYear = Convert.ToInt32(collection["BirthYear"]);
                author.FirstName = collection["FirstName"];
                author.LastName = collection["LastName"];
                author.aid = 0;
                author.save();

                return RedirectToAction("AdminLogin");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult CreateBook()
        {
            return View();

        }

        //
        // POST: /Menu/Create

        [HttpPost]
        public ActionResult CreateBook(FormCollection collection)
        {
            //try
            //{
                SystemLibBL.Book newBook = new SystemLibBL.Book();
                newBook.ISBN = collection["ISBN"];
                newBook.publicationInfo = collection["publicationinfo"];
                newBook.publicationYear = collection["PublicationYear"];
                newBook.SignID = Convert.ToInt32(collection["SignId"]);
                newBook.title = collection["Title"];
                newBook.AuthorFirstName = collection["AuthorFirstName"];
                newBook.AuthorLastName = collection["AuthorLastName"];
                newBook.pages = Convert.ToInt16(collection["Pages"]);
                int response = newBook.save();
                if (response == -4)
                {
                    ViewBag.ErrorMessage = "NoAuthor";
                }

                return RedirectToAction("AdminLogin");
            //}
            //catch
            //{
            //    return View();
            //}
        }

        public ActionResult CreateBorrower()
        {
            return View();

        }

        //
        // POST: /Menu/Create

        [HttpPost]
        public ActionResult CreateBorrower(FormCollection collection)
        {
            try
            {
                SystemLibDTO.UserDTO borrower = new SystemLibDTO.UserDTO();
                
                borrower._Address = collection["Address"];
                borrower._FirstName = collection["FirstName"];
                borrower._LastName = collection["LastName"];
                borrower._Authorization = Convert.ToInt32(collection["Authorization"]);
                if (Convert.ToInt32(collection["CategoryID"]) > 4 || Convert.ToInt32(collection["CategoryID"]) < 1)
                {
                    TempData["ErrorCategoryID"] = "ErrorCategoryID";
                    return RedirectToAction("CreateBorrower", "Menu");
                }

                borrower._CategoryID = Convert.ToInt32(collection["CategoryId"]);
                borrower._Password = collection["Password"];
                borrower._PersonId = collection["PersonId"];
                borrower._Telno = collection["Telno"];

                SystemLibBL.User.createUser(borrower);

                return RedirectToAction("AdminLogin");
            }
            catch
            {
                return View();
            }
        }



        //
        // GET: /Menu/Edit/5

        public ActionResult Edit(int id)
        {
            return View(SystemLibBL.Author.findAuthor(id));
        }

        //
        // POST: /Menu/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                SystemLibBL.Author au = SystemLibBL.Author.findAuthor(id);
                au.BirthYear = Convert.ToInt32(collection["BirthYear"]);
                au.FirstName = collection["FirstName"];
                au.LastName = collection["LastName"];
                au.aid = id;
                au.save();

                return RedirectToAction("AdminLogin");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult EditBook(string ISBN)
        {
            return View(SystemLibBL.Book.findBook(ISBN));
        }

        //
        // POST: /Menu/Edit/5

        [HttpPost]
        public ActionResult EditBook(string ISBN, FormCollection collection)
        {
            //try
            //{
                SystemLibBL.Book bo = SystemLibBL.Book.findBook(ISBN);
                bo.ISBN = ISBN;
                bo.title = collection["Title"];
                bo.SignID = Convert.ToInt32(collection["SignId"]);
                bo.pages = Convert.ToInt16(collection["Pages"]);
                bo.publicationInfo = collection["publicationinfo"];
                bo.publicationYear = collection["PublicationYear"];
                //bo.SignID = Convert.ToInt32(collection["SignId"]);//Ska denna va med här?!?!
                bo.save();

                return RedirectToAction("AdminLogin");
            //}
            //catch
            //{
            //    return View();
            //}
        }

        public ActionResult EditBorrower(string personId)
        {
            return View(SystemLibBL.User.findUser(personId));
        } 

        //[HttpPost]
        //public ActionResult EditBorrower(string personId, FormCollection collection)
        //{
        //    try
        //    {
        //        SystemLibBL.User us = SystemLibBL.User.findUser(personId);
        //        us.PersonId = personId;
        //        us.FirstName = collection["FirstName"];
        //        us.LastName = collection["LastName"];
        //        us.Telno = collection["TelNo"];
        //        us.CategoryID = Convert.ToInt32(collection["CategoryId"]);
        //        us.Address = collection["Address"];
        //        us.Authorization = Convert.ToInt32(collection["Authorization"]);

        //        us.save();

        //        return RedirectToAction("AdminLogin");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

     
        public ActionResult BorrowBook(string ISBN)
        {
            return View(SystemLibBL.Book.findBook(ISBN)); 
        }

        [HttpPost]
        public ActionResult BorrowBook(string ISBN, FormCollection collection)
        {
            try
            {
                SystemLibBL.Book bookToBorrow = SystemLibBL.Book.findBook(ISBN);
                SystemLibBL.User loggedInUser = (SystemLibBL.User)Session["User"];

                SystemLibBL.Book.borrowBook(bookToBorrow.ISBN, loggedInUser.PersonId);

            }
            catch
            {
                return View();
            }

            return RedirectToAction("BorrowerLogin");
        }

        public ActionResult CurrentlyBorrowedBooks()
        {
            SystemLibBL.User loggedInUser = (SystemLibBL.User)Session["User"];


            return View(SystemLibBL.Book.fetchCurrentlyBorrowedBooks(loggedInUser.PersonId));
        }

        public ActionResult AdminCurrentlyBorrowedBooks(string personId)
        {
            SystemLibBL.User selectedUser = SystemLibBL.User.findUser(personId);

            return View("CurrentlyBorrowedBooks", Book.fetchCurrentlyBorrowedBooks(selectedUser.PersonId));
        }

        public ActionResult RenewLoan(string ISBN)
        {
            return View(Book.findBook(ISBN));
        }

        [HttpPost]
        public ActionResult RenewLoan(string ISBN, FormCollection collection)
        {
            try
            {
                SystemLibBL.Book.renewLoan(ISBN);
            }
            catch
            {
                return View();
            }
            User loggedInUser = (User)Session["User"];

            if (loggedInUser.Authorization == 1)
            {
                return RedirectToAction("BorrowerLogin");
            }
            else
            {
                return RedirectToAction("AdminLogin");
            }
        }

        public ActionResult UpdateBorrower(FormCollection collection)
        {
            User userToUpdate = new User();

            userToUpdate.Address = collection["Address"];
            userToUpdate.Authorization = Convert.ToInt32(collection["Authorization"]);

            if (Convert.ToInt32(collection["CategoryID"]) > 4 || Convert.ToInt32(collection["CategoryID"]) < 1)
            {
                TempData["ErrorCategoryID"] = "ErrorCategoryID";
                return RedirectToAction("EditBorrower", "Menu", new { personId = collection["PersonId"].ToString()});
            }
            userToUpdate.CategoryID = Convert.ToInt32(collection["CategoryID"]);
            userToUpdate.FirstName = collection["FirstName"];
            userToUpdate.LastName = collection["LastName"];
            userToUpdate.PersonId = collection["PersonId"].ToString();
            userToUpdate.Telno = collection["Telno"];

            SystemLibBL.User.updateBorrower(userToUpdate);

            return RedirectToAction("AdminLogin", "Menu");
        } 
        // GET: /Menu/Delete/5

        public ActionResult Delete(int id)
        {
            return View(SystemLibBL.Author.findAuthor(id));
        }

        //
        // POST: /Menu/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            //try
            //{
                // TODO: Add delete logic here

                Author.deleteAuthor(id);

                return RedirectToAction("AdminLogin");
            //}
            //catch
            //{
            //    return View();
            //}
        }
        public ActionResult DeleteBook(string ISBN)
        {
            return View(SystemLibBL.Book.findBook(ISBN));
        }

        //
        // POST: /Menu/Delete/5

        [HttpPost]
        public ActionResult DeleteBook(string ISBN, FormCollection collection)
        {
            //try
            //{
                // TODO: Add delete logic here
                Book.deleteBook(ISBN);

                return RedirectToAction("AdminLogin");
            //}
            //catch
            //{
            //    return View();
            //}
        }



        public ActionResult DeleteBorrower(string personId)
        {
            return View(SystemLibBL.User.findUser(personId));
        }

        //
        // POST: /Menu/Delete/5

        [HttpPost]
        public ActionResult DeleteBorrower(string personId, FormCollection collection)
        {
            //try
            //{
                // TODO: Add delete logic here

                SystemLibBL.User.deleteUser(personId);


                return RedirectToAction("AdminLogin");
            //}
            //catch
            //{
            //    return View();
            //}
        }
    }
}
