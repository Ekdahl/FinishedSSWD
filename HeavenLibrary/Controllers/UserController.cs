﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SystemLibBL;

namespace HeavenLibrary.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        
        
        public ActionResult LogIn(FormCollection collection)
        {
            string userName = collection["username"];
            string password = collection["password"];
            SystemLibBL.User us = SystemLibBL.User.LogIn(userName,password);
            if (us != null)
            {
                Session["User"] = us;
                ViewBag.succed = "Oj Jag is an user!!";
                
                 if(Convert.ToInt32(us.Authorization) == 2) //Admin
                 {
                     return RedirectToAction("AdminLogin", "Menu");
                 }
                 else if (Convert.ToInt32(us.Authorization) == 1) // Borrower
                 {
                     return RedirectToAction("BorrowerLogin", "Menu");
                 }
                 else
                 {
                     return RedirectToAction("Index", "Menu");//Regular
                 }
            }
            else 
            {

                string a = "hej hej";
                string b = "på dig";
                string c = a + b;
                ViewBag.ab = c;
                ViewBag.succed = "Nope";
                List<string> list= SystemLibBL.User.test(userName, password);
                ViewBag.salt = list[0];
                ViewBag.pass = list[1];
                ViewBag.passdb = list[2];
                return View();
            }
        }

        public ActionResult LogOut()
        {
            Session["User"] = null;
            ViewBag.showLoginPanel = true;
            return RedirectToAction("Index", "Menu");
        }

        //
        // GET: /User/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /User/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /User/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /User/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /User/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /User/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
