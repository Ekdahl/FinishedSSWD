﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemLibDTO
{
    public class BookDTO
    {
        public string ISBN, Title, PublicationYear, PublicationInfo, AutorFirstName, AuthorLastName;
        public int SignID;
        public Int16 Pages;

        public DateTime ToBeReturnedDate, BorrowDate;

    }
}
