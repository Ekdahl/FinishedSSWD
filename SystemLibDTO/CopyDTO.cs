﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemLibDTO
{
    public class CopyDTO
    {
        public string Barcode, Location, ISBN, Library;
        public int StatusId;

    }
}
