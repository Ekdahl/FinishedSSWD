﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemLibDTO
{
    public class UserDTO
    {
        public string _PersonId, _Password, _FirstName, _LastName, _Address, _Telno, _Salt;
        public int _CategoryID, _Authorization;
    }
}
