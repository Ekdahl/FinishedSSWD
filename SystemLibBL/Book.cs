﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemLibDAL;
using SystemLibDTO;

namespace SystemLibBL
{
    public class Book
    {
        private BookDTO bookDto;

        public Book()
        {
            bookDto = new BookDTO();
        }

        public Book(BookDTO dto)
        {
            bookDto = dto;
        }

        public BookDTO dto
        {
            get { return bookDto; }
            set { bookDto = value; }
        }

        public string title
        {
            get { return bookDto.Title; }
            set { bookDto.Title = value; }
        }

        public string publicationYear
        {
            get { return bookDto.PublicationYear; }
            set { bookDto.PublicationYear = value; }
        }
        public string publicationInfo
        {
            get { return bookDto.PublicationInfo; }
            set { bookDto.PublicationInfo = value; }
        }
        public int SignID
        {
            get { return bookDto.SignID; }
            set { bookDto.SignID = value; }
        }
        public Int16 pages
        {
            get { return bookDto.Pages; }
            set { bookDto.Pages = value; }
        }
        public string ISBN
        {
            get { return bookDto.ISBN; }
            set { bookDto.ISBN = value; }
        }
        public string AuthorFirstName
        {
            get { return bookDto.AutorFirstName; }
            set { bookDto.AutorFirstName = value; }
        }
        public string AuthorLastName
        {
            get { return bookDto.AuthorLastName; }
            set { bookDto.AuthorLastName = value; }
        }

        public DateTime ToBeReturnedDate
        {
            get { return bookDto.ToBeReturnedDate; }
            set { bookDto.ToBeReturnedDate = value; }
        }
       
        public DateTime BorrowDate
        {
            get { return bookDto.BorrowDate; }
            set { bookDto.BorrowDate = value; }
        }
        public int save()
        {
            SystemLibDTO.BookDTO bd = new SystemLibDTO.BookDTO();
            bd.ISBN = this.ISBN;
            bd.Pages = this.pages;
            bd.PublicationInfo = this.publicationInfo;
            bd.PublicationYear = this.publicationYear;
            bd.SignID = this.SignID;
            bd.Title = this.title;
            bd.AutorFirstName = this.AuthorFirstName;
            bd.AuthorLastName = this.AuthorLastName;
            bd.Pages = this.pages;
            int response = SystemLibDAL.dataAccess.saveBook(bd);
          
            return response;
        }

        public static List<Book> getBooks()
        {
            List<BookDTO> bookDtoList = SystemLibDAL.dataAccess.getBooks("SELECT BOOK.*, AUTHOR.FirstName, AUTHOR.LastName FROM BOOK JOIN BOOK_AUTHOR ON BOOK.ISBN = BOOK_AUTHOR.ISBN JOIN AUTHOR ON BOOK_AUTHOR.Aid = AUTHOR.Aid");
            List<Book> bookList = new List<Book>();

            foreach (BookDTO bd in bookDtoList)
            {
                Book newBook = new Book();
                newBook.SignID = bd.SignID;
                newBook.title = bd.Title;
                newBook.publicationInfo = bd.PublicationInfo;
                newBook.publicationYear = bd.PublicationYear;
                newBook.ISBN = bd.ISBN;
                newBook.pages = bd.Pages;
                newBook.AuthorFirstName = bd.AutorFirstName;
                newBook.AuthorLastName = bd.AuthorLastName;
                newBook.dto = bd;
                bookList.Add(newBook);
            }
            return bookList;
        }

        public static int borrowBook(string ISBN, string personId)
        {
            DateTime borrowDate = DateTime.Now;
            DateTime toBeReturned = borrowDate.AddDays(7);


            return SystemLibDAL.dataAccess.borrowBook(ISBN, personId, borrowDate, toBeReturned);
        }

        public static int renewLoan(string ISBN)
        {
            return SystemLibDAL.dataAccess.renewLoan(ISBN);
        }

        public static List<Book>fetchCurrentlyBorrowedBooks(string personId)
        {
            List<BookDTO> bookDTOList = new List<BookDTO>();
            bookDTOList = SystemLibDAL.dataAccess.fetchCurrentlyBorrowedBooks(personId);

            List<Book> bookList = new List<Book>();
            foreach (var bookDTO in bookDTOList)
            {
                Book currentlyBorrowedBook = new Book();
                currentlyBorrowedBook.ISBN = bookDTO.ISBN;
                currentlyBorrowedBook.pages = bookDTO.Pages;
                currentlyBorrowedBook.publicationInfo = bookDTO.PublicationInfo;
                currentlyBorrowedBook.publicationYear = bookDTO.PublicationYear;
                currentlyBorrowedBook.SignID = bookDTO.SignID;
                currentlyBorrowedBook.title = bookDTO.Title;
                bookList.Add(currentlyBorrowedBook);
            }


            return bookList;
        }

        public static Book findBook(string ISBN)
        {
            Book book = new Book();
            BookDTO bd = dataAccess.getBook(ISBN);
            book.ISBN = bd.ISBN;
            book.title = bd.Title;
            book.publicationInfo = bd.PublicationInfo;
            book.publicationYear = bd.PublicationYear;
            book.SignID = bd.SignID;
            book.pages = bd.Pages;
            
            return book;
        }

        public static void deleteBook(string ISBN)
        {
            dataAccess.deleteBook(ISBN);
        }

        public static List<Book> searchBook(string SQL)
        {
            List<Book> bookList = new List<Book>();
            List<BookDTO> bookDtoList = dataAccess.getBooks(SQL);
                
            foreach (BookDTO bd in bookDtoList)
            {
                Book newBook = new Book();
                newBook.SignID = bd.SignID;
                newBook.title = bd.Title;
                newBook.publicationInfo = bd.PublicationInfo;
                newBook.publicationYear = bd.PublicationYear;
                newBook.ISBN = bd.ISBN;
                newBook.pages = bd.Pages;
                newBook.AuthorFirstName = bd.AutorFirstName;
                newBook.AuthorLastName = bd.AuthorLastName;
                newBook.dto = bd;
                bookList.Add(newBook);
            }
            
            return bookList;
        }
    }
}
