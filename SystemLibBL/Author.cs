﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemLibDAL;
using SystemLibDTO;
using System.Data.SqlClient;

namespace SystemLibBL
{
    public class Author
    {
        private AuthorDTO authorDto;

        public Author()
        { 
            authorDto = new AuthorDTO();
        }

        public Author(AuthorDTO dto)
        {
            authorDto = dto;
        }

        public AuthorDTO dto
        {
            get { return authorDto; }
            set { authorDto = value; }
        }

        public int aid
        {
            get { return authorDto._aId; }
            set { authorDto._aId = value; }
        }

        public string FirstName
        {
            get { return authorDto._firstName; }
            set { authorDto._firstName = value; }
        }
        public string LastName
        {
            get { return authorDto._lastName; }
            set { authorDto._lastName = value; }
        }
        public int BirthYear
        {
            get { return authorDto._birthYear; }
            set { authorDto._birthYear = value; }
        }

        public int save()
        {
            SystemLibDTO.AuthorDTO ad = new SystemLibDTO.AuthorDTO();
            ad._aId = this.aid;
            ad._birthYear = this.BirthYear;
            ad._firstName = this.FirstName;
            ad._lastName = this.LastName;
            return SystemLibDAL.dataAccess.saveAuthor(ad);
        }

        public static void deleteAuthor(int id)
        {
            SystemLibDAL.dataAccess.delete(id);
        }

        public static Author findAuthor(int aid)
        {
            AuthorDTO ad = dataAccess.getAuthor(aid);
            Author newAuthor = new Author();
            newAuthor.aid = ad._aId;
            newAuthor.BirthYear = ad._birthYear;
            newAuthor.FirstName = ad._firstName;
            newAuthor.LastName = ad._lastName;
            newAuthor.dto = ad;
            return newAuthor;
        }

        public static List<Author> getAllAuthors()
        {
            List<AuthorDTO> authorDtoList = dataAccess.getAuthors("SELECT * FROM AUTHOR");
            List<Author> authorList = new List<Author>();

            foreach (AuthorDTO ad in authorDtoList)
            {
                Author newAuthor = new Author();
                newAuthor.aid = ad._aId;
                newAuthor.BirthYear = ad._birthYear;
                newAuthor.FirstName = ad._firstName;
                newAuthor.LastName = ad._lastName;
                newAuthor.dto = ad;
                authorList.Add(newAuthor);
            }
            return authorList;
        }

        public static List<Author> searchAuthor(string SQL)
        {
            List<Author> authorList = new List<Author>();
            List<AuthorDTO> authorDtoList = dataAccess.getAuthors(SQL);

            foreach (AuthorDTO ad in authorDtoList)
            {
                Author newAuthor = new Author();
                newAuthor.aid = ad._aId;
                newAuthor.BirthYear = ad._birthYear;
                newAuthor.FirstName = ad._firstName;
                newAuthor.LastName = ad._lastName;
                
                newAuthor.dto = ad;
                authorList.Add(newAuthor);
            }

            return authorList;
        }

       
    }
}
