﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemLibDAL;
using SystemLibDTO;

namespace SystemLibBL
{
    class Copy
    {

        private CopyDTO copyDto;

        public Copy()
        {
            copyDto = new CopyDTO();
        }

        public Copy(CopyDTO dto)
        {
            copyDto = dto;
        }

        public CopyDTO dto
        {
            get { return copyDto; }
            set { copyDto = value; }
        }

        public string Barcode
        {
            get { return copyDto.Barcode; }
            set { copyDto.Barcode = value; }
        }

        public string ISBN
        {
            get { return copyDto.ISBN; }
            set { copyDto.ISBN = value; }
        }
        public string Library
        {
            get { return copyDto.Library; }
            set { copyDto.Library = value; }
        }
        public int StatusId
        {
            get { return copyDto.StatusId; }
            set { copyDto.StatusId = value; }
        }
        public string Location
        {
            get { return copyDto.Location; }
            set { copyDto.Location = value; }
        }

        public static Copy findCopy(string ISBN)
        {
            Copy copy = new Copy();

            return copy;
        }
    }
}
