﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemLibDAL;
using SystemLibDTO;

namespace SystemLibBL
{
    public class User
    {
        private UserDTO userDTO;

        public User()
        { 
            userDTO = new UserDTO();
        }
        public string PersonId { 
            get{ return userDTO._PersonId;} 
            set{ userDTO._PersonId = value;} }
        public string Password { 
            get{return userDTO._Password;}
            set{userDTO._Password = value;}
        }
        public string FirstName{
            get{return userDTO._FirstName;}
            set{userDTO._FirstName = value;} 
        }
        public string LastName{      
            get{return userDTO._LastName;}
            set{userDTO._LastName = value;}  }
        public string Address{
            get{return userDTO._Address;}
            set{userDTO._Address = value;} 
        }
        public string Telno { 
            get{return userDTO._Telno;}
            set{userDTO._Telno = value;} 
        }
        public string Salt { 
            get{return userDTO._Salt;}
            set{userDTO._Salt = value;} 
        }
        public int CategoryID{      
            get{return userDTO._CategoryID;}
            set{userDTO._CategoryID = value;} 
        }

        public int Authorization
        {
            get { return userDTO._Authorization; }
            set { userDTO._Authorization = value; }
        }

        public static int createUser(UserDTO uDTO)
        {
            uDTO._Salt = SystemLibBL.Settings.GenerateSalt();
            uDTO._Password = SystemLibBL.Settings.SecureString(uDTO._Password + uDTO._Salt);
            
            return SystemLibDAL.dataAccess.createUser(uDTO);

        }
        public int save()
        {
            SystemLibDTO.UserDTO ud = new SystemLibDTO.UserDTO();
            ud._PersonId = this.PersonId;
            ud._Address = this.Address;
            ud._Authorization = this.Authorization;
            ud._CategoryID = this.CategoryID;
            ud._FirstName = this.FirstName;
            ud._LastName = this.LastName;
            ud._Telno = this.Telno;
            ud._Password = this.Password;
            
            return SystemLibDAL.dataAccess.saveBorrower(ud);
        }

        public static void updateBorrower(User borrowerToUpdate)
        {
            UserDTO uDTO = new UserDTO();
            uDTO._Address = borrowerToUpdate.Address;
            uDTO._Authorization = borrowerToUpdate.Authorization;
            uDTO._CategoryID = borrowerToUpdate.CategoryID;
            uDTO._FirstName = borrowerToUpdate.FirstName;
            uDTO._LastName = borrowerToUpdate.LastName;
            uDTO._PersonId = borrowerToUpdate.PersonId;
            uDTO._Telno = borrowerToUpdate.Telno;


            dataAccess.updateBorrower(uDTO);

        }
        public static List<User> getAllBorrowers()
        {
            List<UserDTO> uDTOList = dataAccess.getAllBorrowers();

            List<User> borrowerList = new List<User>();
            foreach (var uDTO in uDTOList)
            {
                User borrower = new User();
                borrower.Address = uDTO._Address;
                borrower.Authorization = uDTO._Authorization;
                borrower.CategoryID = uDTO._CategoryID;
                borrower.FirstName = uDTO._FirstName;
                borrower.LastName = uDTO._LastName;
                borrower.PersonId = uDTO._PersonId;
                borrower.Telno = uDTO._Telno;
                borrowerList.Add(borrower);
            }
            return borrowerList;
        }

        public static User findUser(string personId)
        {
            User newUser = new User();
            UserDTO bd = dataAccess.getUser(personId);
            newUser.Address = bd._Address;
            newUser.Authorization = bd._Authorization;
            newUser.CategoryID = bd._CategoryID;
            newUser.FirstName = bd._FirstName;
            newUser.LastName = bd._LastName;
            newUser.PersonId = bd._PersonId;
            newUser.Telno = bd._Telno;
            newUser.userDTO = bd;
            return newUser;
        }

        public static List<User> searchUser(string SQL)
        {
            List<User> userList = new List<User>();
            List<UserDTO> userDtoList = dataAccess.getUsers(SQL);

            foreach (UserDTO bd in userDtoList)
            {
                User newUser = new User();
                newUser.Address = bd._Address;
                newUser.Authorization = bd._Authorization;
                newUser.CategoryID = bd._CategoryID;
                newUser.FirstName = bd._FirstName;
                newUser.LastName = bd._LastName;
                newUser.PersonId = bd._PersonId;
                newUser.Telno = bd._Telno;
                newUser.userDTO = bd;
                userList.Add(newUser);
            }

            return userList;
        }

        public static void deleteUser(string personId)
        {
            //SystemLibDTO.UserDTO ud = new UserDTO();

            //ud._Address = this.Address;
            //ud._Authorization = this.Authorization;
            //ud._CategoryID = this.CategoryID;
            //ud._FirstName = this.FirstName;
            //ud._LastName = this.LastName;
            //ud._Password = this.Password;
            //ud._PersonId = personId;
            //ud._Salt = this.Salt;
            //ud._Telno = this.Telno;

            dataAccess.deleteUser(personId);


        }

        //Används ej
        public static List<string> test(string pid, string pass)
        {
            UserDTO userDTO = dataAccess.getUser(pid);
            User responseUser = new User();

            

            string hashPass = Settings.SecureString(pass + userDTO._Salt);
            List<string> lista = new List<string>();
            lista.Add(userDTO._Salt);
            lista.Add(userDTO._Password);
            lista.Add(hashPass);
            return lista;

        }
        public static User LogIn(string PersonID, string password)
        {
            UserDTO userDTO = dataAccess.getUser(PersonID);
            User responseUser = new User();

            string hashPass = Settings.SecureString(password + userDTO._Salt);
            if (hashPass == userDTO._Password)//dataAccess.ValidateUser(PersonID, hashPass))
            {
                responseUser.PersonId = userDTO._PersonId;
                responseUser.FirstName = userDTO._FirstName;
                responseUser.LastName = userDTO._LastName;
                responseUser.Address = userDTO._Address;
                responseUser.Authorization = userDTO._Authorization;

                return responseUser;
            }
            else
            {
                
                return null;
            }
        }
    }
}
